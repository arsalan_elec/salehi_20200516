package com.example.videodemoapp.domain

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json


data class Video(
    val videoUrl: String?,
    val audioUrlsMap: Map<String, String>,
    val thumbUrl: String?,
    val title: String?
)

data class VideoJson(
    @Json(name = "video_url") val videoUrl: String,
    @Json(name = "audio_urls") val audioUrlsMap: Map<String, String>,
    @Json(name = "thumb") val thumbName: String,
    @Json(name = "title") val title: String
)

data class VideoContainer(
    @Json(name = "videos") val videos: List<VideoJson>
)

// An extension function to convert the json object to one with correct thumb url
fun VideoContainer.toDomainModel(): List<Video> {
    return videos.map {
        Video(
            videoUrl = it.videoUrl,
            audioUrlsMap = it.audioUrlsMap,
            title = it.title,
            thumbUrl = it.videoUrl.run {
                substring(0, lastIndexOf('/') + 1)
            } + it.thumbName
        )
    }
}