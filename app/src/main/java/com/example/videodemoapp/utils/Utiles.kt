package com.example.videodemoapp.utils

import android.content.Context
import android.content.res.Resources
import java.io.IOException


object Utils {

    // Read the json string from the file
    fun getJsonFromFile(
        context: Context,
        fileId: Int
    ): String? {
        val jsonString: String
        jsonString = try {

            val `is` = context.resources.openRawResource(fileId)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, Charsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return jsonString
    }
}