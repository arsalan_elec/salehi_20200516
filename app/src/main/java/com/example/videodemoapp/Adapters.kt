package com.example.videodemoapp

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.videodemoapp.databinding.ItemVideoBinding
import com.example.videodemoapp.domain.Video
import com.facebook.drawee.drawable.ProgressBarDrawable
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util


class VideoRecyclerAdapter(
    private val player: SimpleExoPlayer,
    private val itemClickEvent: RecyclerItemClickEvent
) : // I user ListAdapter to update one item without changing all items
    ListAdapter<Video, VideoRecyclerAdapter.ViewHolder>(object : ItemCallback<Video>() {

        override fun areItemsTheSame(oldItem: Video, newItem: Video): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Video, newItem: Video): Boolean {
            return newItem.videoUrl == oldItem.videoUrl
        }

    }) {

    // Track the selected item
    var currentSelection: Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemVideoBinding>(
            LayoutInflater
                .from(parent.context)
            , R.layout.item_video, parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        // Produces DataSource instances through which media data is loaded.
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
            holder.itemView.context,
            Util.getUserAgent(holder.itemView.context, "video-demo-app")
        )

        // To solve some problem with Mp4 format that caused the video to start late
        val extractorsFactory = DefaultExtractorsFactory()
            .setMp4ExtractorFlags(Mp4Extractor.FLAG_WORKAROUND_IGNORE_EDIT_LISTS)


        val videoUri = Uri.parse(getItem(position).videoUrl)

        // This is the MediaSource representing the video to be played.
        val videoSource: MediaSource =
            ProgressiveMediaSource
                .Factory(dataSourceFactory, extractorsFactory)
                .createMediaSource(videoUri)

        val audioUri = Uri.parse(getItem(position).audioUrlsMap["en"])

        // This is the MediaSource representing the audio to be played.
        val audioSource: MediaSource =
            ProgressiveMediaSource
                .Factory(dataSourceFactory)
                .createMediaSource(audioUri)

        // Merging audio and video sources
        val mergedSource = MergingMediaSource(videoSource, audioSource)

        with(holder.binding) {

            // Binding the video
            video = getItem(position)

            // Set the item selection position holder
            if (currentSelection != position) {
                playerView.visibility = View.INVISIBLE
                thumbnail.visibility = View.VISIBLE
                buttonPlay.visibility = View.VISIBLE
                playerView.player = null
            }

            buttonPlay.setOnClickListener {
                // Prepare the player with the source.
                player.prepare(mergedSource)

                player.playWhenReady = true

                // Hide the Thumbnail and overly Play button
                thumbnail.visibility = View.INVISIBLE
                playerView.visibility = View.VISIBLE

                val oldItem = currentSelection
                currentSelection = position

                // Deselect the last item
                notifyItemChanged(oldItem)

                // Set the player to playerView
                playerView.player = player

                // Show error to the user
                (playerView.player as SimpleExoPlayer).addListener(object : Player.EventListener {
                    override fun onPlayerError(error: ExoPlaybackException) {
                        super.onPlayerError(error)
                        textError.text = error.localizedMessage
                        textError.visibility = View.VISIBLE
                    }
                })
            }

            // Go to fullScreen mode
            val fullScreenBtn = playerView.findViewById<ImageButton>(R.id.exo_fullscreen_enter)
            fullScreenBtn.setOnClickListener {
                itemClickEvent.fullScreenItemClicked(playerView)
            }
        }
    }


    // Our View holder
    class ViewHolder(val binding: ItemVideoBinding) : RecyclerView.ViewHolder(binding.root)

}

interface RecyclerItemClickEvent {
    fun fullScreenItemClicked(playerView: PlayerView)
}

/**
 * Binding a url to simpleDraweeView
 * used to show thumbnails
 */
@BindingAdapter("imageUrl")
fun SimpleDraweeView.setImageUrl(url: String?) {

    val uriPath: Uri = Uri.parse(url)

    setImageURI(uriPath)
    hierarchy.setProgressBarImage(ProgressBarDrawable())
}