package com.example.videodemoapp

import android.app.Application
import com.facebook.common.logging.FLog
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.listener.RequestListener
import com.facebook.imagepipeline.listener.RequestLoggingListener

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        val requestListeners: Set<RequestListener> = HashSet()
        requestListeners.plus(RequestLoggingListener())
        val config: ImagePipelineConfig = ImagePipelineConfig.newBuilder(this)
            // other setters
            .setRequestListeners(requestListeners)
            .build()
        Fresco.initialize(this, config)
        FLog.setMinimumLoggingLevel(FLog.VERBOSE)
    }
}