# Video List Demo App

This App shows a list of videos in a recycler view.

The user can play, pause, seek, and see the video in fullScreen mode without need of re-buffering data.

## Installation

Just clone this repository and put videos.json in the raw folder and build the app in Android Studio.

The raw folder should is in this path:
```bash
/app/src/main/res/raw
```