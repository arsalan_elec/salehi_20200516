package com.example.videodemoapp

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.videodemoapp.databinding.ActivityMainBinding
import com.example.videodemoapp.domain.VideoContainer
import com.example.videodemoapp.domain.toDomainModel
import com.example.videodemoapp.utils.Utils.getJsonFromFile
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.PlayerView
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // One Player will be created for all videos
        val player = SimpleExoPlayer.Builder(this).build()

        // A custom adapter for the recyclerView
        val adapter = VideoRecyclerAdapter(player, object : RecyclerItemClickEvent {

            @SuppressLint("SourceLockedOrientationActivity")
            override fun fullScreenItemClicked(itemPlayerView: PlayerView) {

                // A fullScreen dialog to show the video in fullScreen mode
                val fullScreenDialog =
                    Dialog(this@MainActivity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)

                // Adding PlayerView to the dialog
                val playerView =
                    PlayerView(this@MainActivity).apply {
                        setBackgroundColor(resources.getColor(android.R.color.black))
                    }

                fullScreenDialog.setContentView(
                    playerView,
                    ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )

                // Set the player
                playerView.player = player

                // Changing oriantation to landscape
                requestedOrientation =
                    ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE

                // Show the dialog
                fullScreenDialog.show()

                // Show exit fullScreen button
                val exitFullScreenBtn =
                    playerView.findViewById<ImageButton>(R.id.exo_fullscreen_exit)
                exitFullScreenBtn.visibility = View.VISIBLE

                // Hide enter fullScreen button
                val enterFullscreenBtn =
                    playerView.findViewById<ImageButton>(R.id.exo_fullscreen_enter)
                enterFullscreenBtn.visibility = View.GONE


                // Exit fullScreen mode
                exitFullScreenBtn.setOnClickListener {

                    // Get back to portrait mode
                    requestedOrientation =
                        ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT

                    // Back to mainActivity
                    fullScreenDialog.dismiss()

                    // get back the videoSurfaceView to recycler item
                    player.setVideoSurfaceView((itemPlayerView.videoSurfaceView as SurfaceView))
                }
            }

        })


        // Setup recyclerView
        binding.videoRecycler.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            this.adapter = adapter
        }

        // Initialise the Moshi to parse the json file
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val jsonAdapter: JsonAdapter<VideoContainer> = moshi.adapter(VideoContainer::class.java)

        getJsonFromFile(this, R.raw.videos)?.let {
            val videoContainer: VideoContainer? = jsonAdapter.fromJson(it)
            adapter.submitList(videoContainer?.toDomainModel())
        }

    }
}
